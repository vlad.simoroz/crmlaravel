<?php
return [
    /**
     * Controller messages
     */
    'days_left_by_company'   => ':key Тариф компании :company заканчивается  Date :date',
    'find_model'             => ':model найдено успешно',
    'create_model'           => ':model создано',
    'update_model'           => ':model обновленно',
    'destroy_model'          => ':model удалено',
    'pay'                    => 'Ваш платеж был получен',
    'pay_invoice'            => 'Ваш платеж , по invoice :invoice , был получен',
    'find_all_tag'           => 'Теги, по модели :model, найдены',
    'user_register'          => 'Пользователь зарегестрирован успешно',
    'logout'                 => 'Пользователь вылогинелся успешно',
    'password_reset'         => 'Мы отправили вам ссылку для сброса пароля по электронной почте!',
    'password_change'        => 'Пароль успешно изменен!',
    'img_update'             => 'Картинка в  :model обновленна',
    'find_notification'      => 'Уведомления по компании найдены',



    /**
     * Error messages
     */

    'not_find_model'            => 'Модель { :model } с id { :id } не найдена',
    'model_exist'               => ':model с такими параметрами :param , уже существует',
    'model_not_exist'           => ':model не существует',
    'device_exist_by_code'      => 'Устройство с кодом `code_device` :code_device уже существует',
    'model_not_has_property'    => 'Нет параметров `:property` в моделе :model ',
    'device_type_not_exist'     => 'Устройство , по коду { :type_code } , не найдено',
    'hub_exist_by_code'         => 'Хаб , с таким `code_hub` :code_hub , уже существует',
    'user_email_exist'          => 'Пользователь с таким email :email , уже существует',
    'unauthorised'              => 'Не авторизован',
    'not_find_user_by_email'    => 'Пользователь с таким e-mail :email , не найден',
    'param_field_not_exist'     => 'param field not exist',
    'param_condition_not_exist' => 'param condition not exist',
    'param_not_exist'           => 'param field not exist',
    'not_have_file'             => 'Нет файла',
    'unknown'                   => 'Неизвестный :param',
    'model_not_exist_param'     => 'Модель { :model } с параметром :param_name { :param } , не найдена',

    /**
     * Info message
     */

    'email_not_verified'  => 'Пользователь :user_name ( id :user_id) не подтвердил почту!',
    'reset_token_invalid' => 'Этот токен сброса пароля недействителен',
    'token_lifetime'      => 'срок действия токена истек',
    'max_limit_tariff'    => 'Вы достигли лимита по :param в Вашем тарифном плане',
    'not_have_tariff'     => 'У Вас нет активного тарифного плана',
];
