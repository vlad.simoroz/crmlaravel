<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'login' => 'vladsim',
            'name' => 'Vlad',
            'sername' => 'Simoroz',
            'phone' => '0631303668',
            'email' => 'vlad.simoroz@gmail.com',
            'password' => bcrypt('bwGJk1G4'),
            'company_id' => '1',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now(),
        ]);
    }
}
