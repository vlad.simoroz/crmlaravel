<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'id' => 1,
            'status' => 1,
            'name' => 'VladCompany',
            'website' => 'https://google.com',
            'email' => 'vlad.simoroz@gmail.com',
            'phone' => '0631303668',
            'address' => 'Kiev',
            'account_hold' => 0,
            'date_create' => Carbon\Carbon::today(),
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now(),
        ]);
    }
}
