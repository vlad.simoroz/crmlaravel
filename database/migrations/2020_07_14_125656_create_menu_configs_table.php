<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMenuConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $menu_configs = config('Migrations.menu_config');

        Schema::create('menu_configs', function (Blueprint $table) {
            $table->id();
            $table->string('route');
            $table->string('name');
            $table->integer('company_id');
            $table->timestamps();
        });

        foreach ($menu_configs as $menu_config) {
            DB::table('menu_configs')->insert([
                "id" => $menu_config["id"],
                "route" => $menu_config["route"],
                "name" => $menu_config["name"],
                "company_id" => $menu_config["company_id"],
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_configs');
    }
}
