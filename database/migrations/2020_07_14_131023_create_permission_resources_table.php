<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePermissionResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission_resources = config('Migrations.permission_resources');

        Schema::create('permission_resources', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('role_id');
            $table->string('model');
            $table->integer('can_read');
            $table->integer('can_write');
            $table->timestamps();
        });

        foreach ($permission_resources as $permission_resource) {
            DB::table('permission_resources')->insert([
                "id" => $permission_resource["id"],
                'name' => $permission_resource["name"],
                'role_id' => $permission_resource["role_id"],
                'model' => $permission_resource["model"],
                'can_read' => $permission_resource["can_read"],
                'can_write' => $permission_resource["can_write"],
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_resources');
    }
}
