<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('login');
            $table->string('name');
            $table->string('sername')->nullable();
            $table->integer('company_id');
            $table->string('phone')->nullable();
            $table->string('position')->nullable();
            $table->string('email')->unique();
            $table->smallInteger('status')->default(1);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('role_id');
            $table->string('img')->nullable();
            $table->integer('telegram_chat_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([
            'id' => 4,
            'login' => 'vladsim',
            'name' => 'Vlad',
            'sername' => 'Simoroz',
            'phone' => '0631303668',
            'position' => 'Head of It Department',
            'email' => 'vlad.simoroz@gmail.com',
            'password' => bcrypt('bwGJk1G4'),
            'company_id' => '1',
            'role_id'=>1,
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
