<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class PermissionObject extends Model
{
    protected $fillable = [
        'id', 'name', 'permission_resources_id','can_read','can_write','description','object_id'
    ];

    protected $with = ['permission_resources'];


    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function permission_resources(){
        return $this->belongsTo(PermissionResources::class);
    }
}
