<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guard_name = 'api';

    protected $fillable = [
        'id', 'name', 'status','website','email','phone','address','account_hold','date_create'
    ];

    protected $with = ['fs'];

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    protected $casts = [
        'date_create' => 'date:Y-m-d',
    ];

    public function user(){
        return $this->hasMany(User::class );
    }

    public function fs()
    {
        return $this->morphMany(Fs::class, 'fstable');
    }
}
