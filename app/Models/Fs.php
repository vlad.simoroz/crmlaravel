<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Fs extends Model
{
    protected $fillable = [
        'id', 'name', 'type','extension','size','dir','company_id',
    ];

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function fstable()
    {
        return $this->morphTo();
    }
    public function company(){
        return $this->belongsTo(Company::class,'company_id');
    }
}
