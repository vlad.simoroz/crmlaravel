<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class PermissionResources extends Model
{
    protected $fillable = [
        'id', 'name', 'role_id','model','can_read','can_write'
    ];

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function permission_object(){
        return $this->hasMany(PermissionObject::class);
    }
}
