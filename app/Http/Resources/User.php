<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function toArray($result)
    {
        $res = collect($result);
        $result = $res->map(function ($item, $key) {
            foreach ($item as $k => $v){
                if (is_array($v)){
                    foreach ($v as $ke => $ve){

                        if (is_null($ve)){
                            $item[$k][$ke] = '';
                        }
                    }
                }else{
                    if (is_null($v)){
                        $item[$k] = '';
                    }
                }
            }
            return $item;
        });
        $response = [
            'user'    => $result,
        ];
        return response($response)
            ->withHeaders([
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
                'Access-Control-Allow-Headers' => 'Authorization, Content-Type',
                'Access-Control-Allow-Credentials' => 'True',
            ]);
    }
}
