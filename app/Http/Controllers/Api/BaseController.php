<?php

namespace App\Http\Controllers\Api;

use App\Models\PermissionResources;
use App\Repositories\PermissionResourcesRepository;
use App\Services\LogService;
use Illuminate\Http\JsonResponse;

class BaseController
{
    /**
     *  Модель для логирования
     */
    public $model;
    /**
     * @var array Хедеры для отправки ответа
     */
    public $headers = [
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers' => 'Authorization, Content-Type',
        'Access-Control-Allow-Credentials' => 'True',
    ];

    public function checkPermissionRead($object_id = null,$model=null)
    {
        if (is_null($model)){
            $model = $this->model;
        }
        $permissions_res_repo = new PermissionResourcesRepository(PermissionResources::class);
        $check_permissions = $permissions_res_repo->checkByRoleModelRead($model,$object_id);

        if (!$check_permissions){
            $this->sendError('Find error',__('messages.not_have_permissions_read_in_model', [ 'model' => $this->model ] ),1);
        }

        return $check_permissions;
    }

    public function checkPermissionWrite($object_id = null,$model=null)
    {
        if (is_null($model)){
            $model = $this->model;
        }
        $permissions_res_repo = new PermissionResourcesRepository(PermissionResources::class);
        $check_permissions = $permissions_res_repo->checkByRoleModelWrite($model,$object_id);

        if (!$check_permissions){
            $this->sendError('Find error',__('messages.not_have_permissions_write_in_model', [ 'model' => $this->model ] ),1);
        }

        return true;
    }

    /**
     * success response method.
     *
     * @param $result
     * @param $message
     * @return JsonResponse
     */
    public function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];

        $log_service = new LogService();
        $log_service->log($response, $this->model);

        return response()->json($response,200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT)->withHeaders($this->headers);
    }

    public function sendResponseNoLog($result, $message)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];

        return response()->json($response,200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT)->withHeaders($this->headers);
    }
    /**
     * Метод для отправки ответа обэктом
     * @param $result
     * @param $message
     * @return JsonResponse
     */
    public function sendResponseObj($result, $message)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];

        $log_service = new LogService();
        $log_service->log($response, $this->model);

        return response()->json($response,200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT)->withHeaders($this->headers);
    }

    /**
     * Метод для отправки модели юзера
     * @param $result
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function sendResponseUser($result)
    {
        $res = collect($result);

        $result = $res->map( function ($item, $key) {
            foreach ($item as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $ke => $ve) {
                        if (is_null($ve)) {
                            $item[$k][$ke] = '';
                        }
                    }
                } else {
                    if (is_null($v)) {
                        $item[$k] = '';
                    }
                }
            }
            return $item;
        });

        if ( !isset( $result[0] ) ) {
            $response['user'] = [];
        }

        $response['user'] = $result[0];
        $response['success'] = true;


//        $log_service = new LogService();
//        $log_service->log($response,$this->model);

        return response($response)->withHeaders($this->headers);
    }

    /**
     * return error response.
     *
     * @return JsonResponse
     */
    public function sendError($error, $errorMessages = [], $repo = null, $code = 200)
    {
        $response = [
            'success' => false,
            'message' => $errorMessages,
        ];

        if( !empty($errorMessages) ) {
            $response['data'] = [];
        }
        if ( $repo != null) {
            abort( response()->json($response, $code)->withHeaders($this->headers) );
        }
        return response()->json($response, $code)->withHeaders($this->headers);
    }
}
