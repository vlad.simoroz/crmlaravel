<?php

namespace App\Http\Controllers\Api;


use App\Helpers\ApiHelper;
use App\Models\PermissionObject;
use App\Models\PermissionResources;
use App\Repositories\PermissionObjectRepository;
use App\Repositories\PermissionResourcesRepository;
use Illuminate\Http\Request;

class PermissionResourcesController extends BaseController
{
    public $model = PermissionResources::class;
    protected $repo;

    /**
     * RoleController constructor.
     */
    public function __construct()
    {
        $this->repo = new PermissionResourcesRepository($this->model);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * Метод для получения всех
     */
    public function index()
    {
        $role = $this->repo->all();
        return $this->sendResponse($role, __('messages.find_model', [ 'model' => 'Permission Resources' ] ));
    }

    public function getByRole($id){
        $permissions = $this->repo->allByRole($id);
        $permission_object_repo = new PermissionObjectRepository(PermissionObject::class);
        foreach ($permissions as $permission) {
            $result_object = [];
            $result_object_array_id = [];
            $permission_objects = $permission_object_repo->getAllByResources($permission->id);
            foreach ($permission_objects as $permission_object) {
                $result_object_permission = (object) [
                    "id" => $permission_object->object_id,
                    "name" => $permission_object->name,
                    "can_read" => $permission_object->can_read,
                    "can_write" => $permission_object->can_write,
                ];
                array_push($result_object,$result_object_permission);
                array_push($result_object_array_id,$permission_object->object_id);
            };


            $results = $permission->model::select('id','name')->whereNotIn('id',$result_object_array_id)->where('company_id', ApiHelper::getCompanyId())->get();

            foreach ($results as $result) {
                $res_array = (object) [
                    "id" => $result->id,
                    "name" => $result->name,
                ];

                if ($permission->can_read == 1){
                    $res_array->can_read = 1;
                }else{
                    $res_array->can_read = 0;
                }

                if ($permission->can_write == 1){
                    $res_array->can_write = 1;
                }else{
                    $res_array->can_write = 0;
                }
                array_push($result_object,$res_array);
            }
            $permission->permission_object = $result_object;
        }

        return $this->sendResponse($permissions, __('messages.find_model', [ 'model' => 'Permission Resources' ] ));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Метод для создания
     */
    public function store(Request $request)
    {
        $result = [];
        foreach ($request->all() as $data) {
            $data = $this->repo->validateCustom($data);
            $role = $this->repo->checkAndCreate($data);
            array_push($result,$role);
        }

        return $this->sendResponseObj($result,__('messages.create', [ 'model' => 'Permission Resources' ] ));
    }

    public function show($id)
    {
        $role = $this->repo->find($id);

        return $this->sendResponseObj($role, __('messages.find_model', [ 'model' => 'Permission Resources' ] ));
    }

    public function updateMass(Request $request)
    {
        $result = [];
        foreach ($request->except(['id']) as $input) {
            $this->repo->rule([
                "id" =>"required|integer",
                "name" => "min:1",
                "role_id" => "integer",
                "can_read" => "integer",
                "can_write" => "integer",
            ]);
            $data = $this->repo->validateCustom($input);

            $role = $this->repo->checkAndUpdate($input["id"],$data);
            array_push($result,$role);
        }
        return $this->sendResponseObj($result,__('messages.update_model', [ 'model' => 'Permission Resources' ] ));
    }

    public function update(Request $request , $id)
    {
        $this->repo->rule([
            "name" => "min:1",
            "can_read" => "integer",
            "can_write" => "integer",
        ]);
        $data = $this->repo->validate($request);
        $role = $this->repo->checkAndUpdate($id,$data[0]);

        return $this->sendResponseObj($role,__('messages.update_model', [ 'model' => 'Permission Resources' ] ));
    }

    public function destroy($id)
    {
        $role = $this->repo->checkAndDestroy($id);

        return $this->sendResponse($role,__('messages.destroy_model', [ 'model' => 'Permission Resources' ]));
    }

}
