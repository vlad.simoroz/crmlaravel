<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Base;
use App\Models\RequestLog;
use App\Models\User;
use App\Repositories\RequestLogRepository;
use Illuminate\Http\Request;

class RequestLogController extends BaseController
{
    public $model = RequestLog::class;
    private $repo;

    /**
     * LocationController constructor.
     * Проверяет есть ли у юзера права на методы контроллера
     */
    function __construct()
    {
        $this->repo = new RequestLogRepository($this->model);
//        $this->checkPermission('request_log');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * Получить коллекцию всех логов по компании
     **/
    public function index()
    {
        $logs = $this->repo->allByCompany();

        foreach ($logs as $log) {
            $user = User::find($log->user_id);
            $log->user_name = $user->name;
        }

        return $this->sendResponse(new Base($logs), __('messages.find_model', [ 'model' => 'Request log' ] ));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Метод для получения сущьностей по фильтрам
     */
    public function filter(Request $request)
    {
        $logs = $this->repo->getByFilter($request);

        foreach ($logs as $log) {
            $user = User::find($log->user_id);
            $log->user_name = $user->name;
        }

        return $this->sendResponse($logs, __('messages.find_model', [ 'model' => 'Request log' ] ));
    }
}
