<?php

namespace App\Http\Controllers\Api;

use App\Models\PermissionObject;
use App\Repositories\PermissionObjectRepository;
use Illuminate\Http\Request;

class PermissionObjectController extends BaseController
{
    public $model = PermissionObject::class;
    protected $repo;

    /**
     * RoleController constructor.
     */
    public function __construct()
    {
        $this->repo = new PermissionObjectRepository($this->model);
    }

    public function index()
    {
        $role = $this->repo->all();
        return $this->sendResponse($role, __('messages.find_model', [ 'model' => 'Permission Object' ] ));
    }

    public function store(Request $request)
    {
        $data = $this->repo->validate($request);
        $role = $this->repo->checkAndCreate($data);

        return $this->sendResponseObj($role,__('messages.create', [ 'model' => 'Permission Object' ] ));
    }

    public function show($id)
    {
        $role = $this->repo->find($id);

        return $this->sendResponseObj($role, __('messages.find_model', [ 'model' => 'Permission Object' ] ));
    }

    public function update(Request $request, $id)
    {
        $this->repo->rule([
            "name" => "min:1",
            "permission_resources_id" => "integer",
            "can_read" => "integer",
            "can_write" => "integer",
            "object_id" => "integer",
        ]);
        $data = $this->repo->validate($request);
        $role = $this->repo->checkAndUpdate($id,$data);

        return $this->sendResponseObj($role,__('messages.update_model', [ 'model' => 'Permission Object' ] ));
    }

    public function destroy($id)
    {
        $role = $this->repo->checkAndDestroy($id);

        return $this->sendResponse($role,__('messages.destroy_model', [ 'model' => 'Permission Object' ]));
    }

}
