<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\AppConfigRepository;
use Illuminate\Http\Request;

class AppConfigController extends BaseController
{
    protected $repo;
    /**
     * DeviceController constructor.
     * Проверяет есть ли у юзера права на методы контроллера
     */
    function __construct()
    {
        $this->repo = new AppConfigRepository();
    }

    public function index()
    {
        $result["menu_config"]["item"] = $this->repo->getConfigApp();

        return $this->sendResponse($result, __('messages.find_model', [ 'model' => 'AppConfig' ] ));
    }
}
