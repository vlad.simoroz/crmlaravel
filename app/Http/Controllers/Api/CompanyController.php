<?php


namespace App\Http\Controllers\Api;


use App\Helpers\ApiHelper;
use App\Models\Company;
use App\Repositories\CompanyRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CompanyController extends BaseController
{
    public $model = Company::class;

    private $repo;

    /**
     * CompanyController constructor.
     * @param CompanyRepository $companyRepository
     */
    public function __construct()
    {
        $this->repo = new CompanyRepository($this->model);
        //        $this->checkPermission('company');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * Возвращает обект компании
     */
    public function index()
    {
        $company = $this->repo->find(ApiHelper::getCompanyId());

        return $this->sendResponseObj($company , __('messages.find_model', [ 'model' => 'Company' ] ));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * Получить список всех компаний
     */
    public function getAll(){
        $company = $this->repo->all();
        return $this->sendResponseObj($company , __('messages.find_model', [ 'model' => 'Company' ] ));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Создание новой компании
     * Возвращает обект компании
     */
    public function store(Request $request)
    {
        $data = $this->repo->validate($request);
        $data = $this->repo->addToData($data,["date_create"=>Carbon::now()]);
        $company = $this->repo->checkAndCreate($data);

        return $this->sendResponseObj( $company , __('messages.create', [ 'model' => 'Company' ] ));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * Метод для получения информации по компании
     * Возвращает обект компании
     */
    public function show($id)
    {
        $company = $this->repo->find($id);

        return $this->sendResponseObj( $company , __('messages.find_model', [ 'model' => 'Company' ] ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * Метод для обновления информации по компании
     * Возвращает обект компании
     */
    public function update(Request $request, $id)
    {
        $data = $this->repo->validate($request);
        $company = $this->repo->checkAndUpdate($id,$data);

        return $this->sendResponseObj( $company , __('messages.update_model', [ 'model' => 'Company' ]) );
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $company = $this->repo->checkAndDestroy($id);

        return $this->sendResponse( $company , __('messages.destroy_model', [ 'model' => 'Company' ]) );
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_img(Request $request , $id)
    {
        $result = $this->repo->updateImg($request , $id);

        return $this->sendResponseObj($result,__('messages.img_update', [ 'model' => 'Company' ] ));
    }
}
