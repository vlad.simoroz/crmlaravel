<?php


namespace App\Http\Controllers\Api;


use App\Helpers\ApiHelper;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends BaseController
{
    public $model = User::class;

    private $repo;

    public function __construct()
    {
        $this->repo = new UserRepository($this->model);
    }

    public function current()
    {
        $user = User::find(Auth::id());

        return response()->json(["user"=>$user]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * Возвращает обект компании
     */
    public function index()
    {
        $company = $this->repo->allByCompany();

        return $this->sendResponseObj($company , __('messages.find_model', [ 'model' => 'User' ] ));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Создать нового юзера
     * Принимает данные для создания юзера
     * Возвращает обект юзера
     */
    public function store(Request $request)
    {
        $input_data = $this->repo->validate($request);
        $data = $this->repo->addToData($input_data,[ 'password' => Hash::make($input_data["password"]) , 'company_id' => ApiHelper::getCompanyId() ]);
        $user = $this->repo->checkAndCreate($data);

        return $this->sendResponseObj($user,__('messages.create', [ 'model' => 'User' ] ));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * Получить юзера по id
     */
    public function show($id)
    {
        $user = $this->repo->findByCompany($id);

        return $this->sendResponseObj($user,__('messages.find_model', [ 'model' => 'User' ] ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * Метод для обновления информации по юзеру
     * Возвращает обект юзера
     */
    public function update(Request $request , $id)
    {
        $this->repo->rule([
            "email" => "email:rfc,dns"
        ]);
        $data = $this->repo->validate($request);
        $user = $this->repo->checkAndUpdate($id,$data);

        return $this->sendResponseObj($user,__('messages.update_model', [ 'model' => 'User' ] ));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * Метод для удаления
     */
    public function destroy($id)
    {
        $user = $this->repo->checkAndDestroy($id);

        return $this->sendResponse($user,__('messages.destroy_model', [ 'model' => 'User' ]));
    }

    public function update_img(Request $request , $id)
    {
        $result = $this->repo->updateImg($request , $id);

        return $this->sendResponseObj($result,__('messages.img_update', [ 'model' => 'User' ] ));
    }
}
