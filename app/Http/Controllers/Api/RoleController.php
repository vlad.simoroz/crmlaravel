<?php

namespace App\Http\Controllers\Api;


use App\Helpers\ApiHelper;
use App\Models\Role;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;

class RoleController extends BaseController
{
    public $model = Role::class;
    protected $repo;

    /**
     * RoleController constructor.
     */
    public function __construct()
    {
        $this->repo = new RoleRepository($this->model);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * Метод для получения всех ролей
     */
    public function index()
    {
        $role = $this->repo->allCustom();
        return $this->sendResponse($role, __('messages.find_model', [ 'model' => 'Role' ] ));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *  Метод для получения роли по id
     */
    public function show($id)
    {
        $role = $this->repo->findCustom($id);

        return $this->sendResponseObj($role, __('messages.find_model', [ 'model' => 'Role' ] ));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Метод для создания роли
     */
    public function store(Request $request)
    {
        $input_data = $this->repo->validate($request);
        $data = $this->repo->addToData($input_data,[ 'status' => 1 ,'company_id' => ApiHelper::getCompanyId()]);
        $role = $this->repo->checkAndCreate($data);

        return $this->sendResponseObj($role,__('messages.create', [ 'model' => 'Role' ] ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * Метод для обновления роли
     */
    public function update(Request $request, $id)
    {
        $data = $this->repo->validate($request);
        $role = $this->repo->checkAndUpdate($id,$data);

        return $this->sendResponseObj($role,__('messages.update_model', [ 'model' => 'Role' ] ));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * Метод для удаления роли
     */
    public function destroy($id)
    {
        $role = $this->repo->checkAndDestroy($id);
        return $this->sendResponse($role,__('messages.destroy_model', [ 'model' => 'Role' ]));
    }
}
