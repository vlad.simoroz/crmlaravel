<?php


namespace App\Repositories;


use App\Helpers\ApiHelper;
use Illuminate\Http\Request;

class RequestLogRepository extends BaseRepository
{
    /**
     * PanelRepository constructor.
     * @param $model
     */
    public function __construct($model)
    {
        /**
         * Присвоение модели с которой будет работа
         */
        $this->model = $model;
        /**
         * Инициализация правил для валидации
         */
        $this->rule();
        /**
         * Инициализация BaseController для отправки ответов
         */
        $this->initialize_response();
    }

    /**
     * @param null $rule
     * установка правил валидации
     */
    public function rule($rule = null)
    {
        if ($rule == null) {
            $this->rule = [ "name" => "required" ];
        }else{
            $this->rule = $rule;
        }
    }

    /**
     * @param Request $data
     * @return Illuminate\Database\Eloquent\Model
     * Метод для поиска  по фильтрам
     */
    public function getByFilter(Request $data)
    {
        $input = $data->all();

        $query = $this->model::query();
        $query -> where( 'company_id' , ApiHelper::getCompanyId() );

        $limit = 10;

        if( isset($input["limit"]) ) {
            $limit = $input["limit"];
            unset($input["limit"]);
        }

        if ( isset($input["date_range"]) ) {
            $query->whereBetween('created_at', $input["date_range"]);
            unset($input["date_range"]);
        }

        foreach ( $input as $params_key => $params ) {
            $query -> whereIn($params_key,$params);
        }

        $query ->limit($limit)->orderBy('id', 'desc');

        $log = $query -> get();

        return $log;
    }
}
