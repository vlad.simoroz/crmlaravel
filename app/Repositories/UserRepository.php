<?php


namespace App\Repositories;


use App\Helpers\ApiHelper;
use App\Http\Controllers\Api\BaseController;
use App\Models\Fs;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class UserRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     * @param $model
     */
    public function __construct($model)
    {
        /**
         * Присвоение модели с которой будет работа
         */
        $this->model = $model;
        /**
         * Инициализация правил для валидации
         */
        $this->rule();
        /**
         * Инициализация BaseController для отправки ответов
         */
        $this->initialize_response();
    }

    /**
     * @param null $rule
     * установка правил валидации
     */
    public function rule($rule = null)
    {
        if ($rule == null) {
            $this->rule = [
                "name"          => "required",
                "password"      => "required|min:6",
                "email"         => "required",
            ];
        }else{
            $this->rule = $rule;
        }
    }

    /**
     * @param $data
     * @return Illuminate\Database\Eloquent\Model
     * Метод для проверки и создания
     */
    public function checkAndCreate($data)
    {
        if($this->model::where('login',$data["username"])->count()>0) {
            return $this->response->sendError('Find error',__('messages.model_exist', [ 'model' => 'User' ,'param' => 'email '.$data["email"] ] ),1);
        }
        $user = $this->create($data);

        return $user;
    }

    /**
     * @param $id
     * @param $data
     * @return Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse
     * Метод для проверки и обновления
     */
    public function checkAndUpdate($id, $data)
    {
//        if ($this->model::where('id',$id)->where('email',$data["email"])->where('company_id',ApiHelper::getCompanyId())->count() === 0){
//            $res = new BaseController();
//            $res->model = $this->model;
//            return $res->sendError('Find error',__('messages.not_find_model', [ 'model' => 'User' ] ),1);
//        }
        $user = $this->update($id,$data);

        return $user;
    }

    /**
     * @param $id
     * @return Illuminate\Database\Eloquent\Model
     * Метод для проверки и удаления
     */
    public function checkAndDestroy($id)
    {
        $user = $this->destroy($id);
        return $user;
    }

    /**
     * @param int $company_id
     * @return mixed
     * посчитать юзеров по компании
     */
    public function countByCompany(int $company_id)
    {
        $count = $this->model::where('company_id' , $company_id)->count();

        return $count;
    }

    public function updateImg($request , $id)
    {
        $input = $request->file;
        if ($input) {
            $check_file = $this->model::find($id);
            $path = $request->file->store('images','public');
            if ($check_file->fs->isNotEmpty()){
                Storage::delete($check_file->fs->first()->dir);
                $check_file->fs->first()->name = $request->file->getClientOriginalName();
                $check_file->fs->first()->type = $request->file->getClientMimeType();
                $check_file->fs->first()->extension = $request->file->extension();
                $check_file->fs->first()->size = Storage::size($path);
                $check_file->fs->first()->dir = $path;
                $check_file->fs->first()->company_id = ApiHelper::getCompanyId();
                $check_file->fs->first()->save();
                return $check_file->fs;
            }else{
                $fs_new = new Fs();
                $fs_new->name = $request->file->getClientOriginalName();
                $fs_new->type = $request->file->getClientMimeType();
                $fs_new->extension = $request->file->extension();
                $fs_new->size = Storage::size($path);
                $fs_new->dir = $path;
                $fs_new->company_id = ApiHelper::getCompanyId();
                $check_file->fs()->save($fs_new);
                $screen_file = $this->model::find($id)->fs->first();
                return $screen_file;
            }
        }else{
            return $this->response->sendError('Find error',__('messages.not_have_file'),1);
        }
    }
}
