<?php

namespace App\Repositories;

use App\Helpers\ApiHelper;
use App\Models\Fs;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class CompanyRepository extends BaseRepository
{
    /**
     * CompanyRepository constructor.
     * @param $model
     */
    public function __construct($model)
    {
        /**
         * Присвоение модели с которой будет работа
         */
        $this->model = $model;
        /**
         * Инициализация правил для валидации
         */
        $this->rule();
        /**
         * Инициализация BaseController для отправки ответов
         */
        $this->initialize_response();
    }

    /**
     * Метод для установки правил валидации
     */
    public function rule($rule = null)
    {
        if ($rule == null) {
            $this->rule = ["name" => "required",];
        }else{
            $this->rule = $rule;
        }
    }

    /**
     * @param $data
     * @return Illuminate\Database\Eloquent\Model
     * Метод для проверок и создания компании
     */
    public function checkAndCreate($data)
    {
        /**
         * Проверяем нет ли такой компании по имени
         */
        if($this->model::where('name',$data["name"])->count()>0) {
            return $this->response->sendError('Find error', __('messages.model_exist', [ 'model' => 'Company', 'param'=> 'name' ] ) ,1);
        }
        $model_obj = $this->create($data);

        return $model_obj;
    }

    /**
     * @param $id
     * @param $data
     * @return Illuminate\Database\Eloquent\Model
     * Проверка и обновление компании
     */
    public function checkAndUpdate($id, $data)
    {
        $company = $this->update($id,$data,true);

        return $this->model::find($id);
    }

    /**
     * @param $id
     * @return Illuminate\Database\Eloquent\Model
     * Метод для удаления компании
     */
    public function checkAndDestroy($id)
    {
        $company = $this->destroy($id);

        return $company;
    }

    public function updateImg($request , $id)
    {
        $input = $request->file;
        if ($input) {
            $check_file = $this->model::find($id);
            $path = $request->file->store('images','public');
            if ($check_file->fs->isNotEmpty()){
                Storage::delete($check_file->fs->first()->dir);
                $check_file->fs->first()->name = $request->file->getClientOriginalName();
                $check_file->fs->first()->type = $request->file->getClientMimeType();
                $check_file->fs->first()->extension = $request->file->extension();
                $check_file->fs->first()->size = Storage::size($path);
                $check_file->fs->first()->dir = $path;
                $check_file->fs->first()->company_id = ApiHelper::getCompanyId();
                $check_file->fs->first()->save();
                return $check_file->fs;
            }else{
                $fs_new = new Fs();
                $fs_new->name = $request->file->getClientOriginalName();
                $fs_new->type = $request->file->getClientMimeType();
                $fs_new->extension = $request->file->extension();
                $fs_new->size = Storage::size($path);
                $fs_new->dir = $path;
                $fs_new->company_id = ApiHelper::getCompanyId();
                $check_file->fs()->save($fs_new);
                $screen_file = $this->model::find($id)->fs->first();
                return $screen_file;
            }
        }else{
            return $this->response->sendError('Find error',__('messages.not_have_file'),1);
        }
    }
}
