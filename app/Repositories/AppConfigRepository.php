<?php


namespace App\Repositories;


use App\Helpers\ApiHelper;
use App\Http\Controllers\Api\PermissionResourcesController;
use App\Models\MenuConfig;
use App\Models\PermissionResources;
use Illuminate\Support\Facades\Auth;

class AppConfigRepository
{
    public function getConfigApp()
    {
        $result = [];
        $per_contr = new PermissionResourcesController();

        $permission_menus = $per_contr->getByRole(ApiHelper::getUserRoleById())->original["data"];

        foreach ($permission_menus as $permission) {
            if ($permission->name == 'Menu') {
                foreach ($permission->permission_object as $permission_object) {
                    if ($permission_object->can_read === 1) {
                        array_push($result,$permission_object->id);
                    }
                }
            }
        }

        $menu_result = MenuConfig::whereIn('id',$result)->get();

        return $menu_result;
    }
}
