<?php


namespace App\Repositories;


use App\Helpers\ApiHelper;
use App\Models\PermissionObject;
use App\Models\Role;
use Illuminate\Support\Facades\Validator;

class PermissionResourcesRepository extends BaseRepository
{
    public function __construct($model)
    {
        /**
         * Присвоение модели с которой будет работа
         */
        $this->model = $model;
        /**
         * Инициализация правил для валидации
         */
        $this->rule();
        /**
         * Инициализация BaseController для отправки ответов
         */
        $this->initialize_response();
    }

    /**
     * @param null $rule
     * установка правил валидации
     */
    public function rule($rule = null)
    {
        if ($rule == null) {
            $this->rule = [
                "name" => "required",
                "model" => "required",
                "role_id" => "required|integer",
                "can_read" => "required|integer",
                "can_write" => "required|integer",
            ];
        }else{
            $this->rule = $rule;
        }
    }

    public function checkAndCreate($data)
    {
        $check_permission = $this->model::where('model',$data["model"])->where('role_id',$data["role_id"])->count();
        if ($check_permission > 0) {
            return $this->response->sendError('Find error',__('messages.model_exist', [ 'model' => 'Permission Resources', 'param'=>' model '.$data["model"].' and role_id '.$data["role_id"] ] ),1);
        }

        $role_repo = new RoleRepository(Role::class);
        $check_role = $role_repo->find($data["role_id"]);

        $permission = $this->create($data);

        $result_all_permission_object = [];

        if ( isset($data["permissions_object"]) ) {
            foreach ($data["permissions_object"] as  $permissions_object ) {
                $data_create = [
                    "name" => $permission->role->name."-".$permission->name."-".$permissions_object["object_id"],
                    "permission_resources_id" => $permission->id,
                    "can_read" => $permissions_object["can_read"],
                    "can_write" => $permissions_object["can_write"],
                    "object_id" => $permissions_object["object_id"],
                ];
                $permission_object_repo = new PermissionObjectRepository(PermissionObject::class);
                $create_permission_object = $permission_object_repo->checkAndCreate($data_create);
                unset($create_permission_object->permission_resources);
                array_push($result_all_permission_object,$create_permission_object);
            }
        }

        $result = $this->find($permission->id);

        $result->permission_object = $result_all_permission_object;

        return $result;
    }

    public function allByRole($id)
    {
        $permissions = $this->model::where('role_id',$id)->get();
        return $permissions;
    }

    public function checkAndUpdate($id, array $data)
    {
        $this->find($id);

        $permission = $this->update($id, $data,true);

        $permission_object_repo = new PermissionObjectRepository(PermissionObject::class);
        $clear_permissions_object = $permission_object_repo->deleteAllByResources($id);

        $result_all_permission_object = [];

        if ( $permission->can_read == 2 || $permission->can_write == 2 ) {

            foreach ($data["permission_object"] as  $permissions_object ) {

                if ($permissions_object["can_read"] == 1 || $permissions_object["can_write"] == 1) {
                    $data_create = [
                        "name" => $permissions_object["name"],
                        "permission_resources_id" => $permission->id,
                        "can_read" => $permissions_object["can_read"],
                        "can_write" => $permissions_object["can_write"],
                        "object_id" => $permissions_object["id"],
                    ];
                    $create_permission_object = $permission_object_repo->checkAndCreate($data_create);
                    unset($create_permission_object->permission_resources);
                    array_push($result_all_permission_object,$create_permission_object);
                }
            }
        }

        $result = $this->find($id);

        $result->permission_object = $result_all_permission_object;

        return $result;
    }

    public function checkAndDestroy($id)
    {
        $permission = $this->destroy($id,true);

        return $permission;
    }

    public function checkByRoleModelRead($model,$object_id)
    {
        $user_role_id = ApiHelper::getUserRoleById();
        $permissions = $this->model::where('role_id',$user_role_id)
            ->where('model',$model)
            ->where(function ($q){
                $q->where('can_read',1)
                    ->orWhere('can_read',2);
            })
            ->first();

        if ($permissions){
            if ($permissions->can_read == 2){
                $permission_object_repository = new PermissionObjectRepository(PermissionObject::class);
                $check_permission_objects = $permission_object_repository->checkRead($permissions->id,$object_id);
                $object_id_res = [];
                if (is_bool($check_permission_objects)) {
                    return $check_permission_objects;
                }else{
                    foreach ($check_permission_objects as $check_permission_object) {
                        array_push($object_id_res,$check_permission_object->object_id);
                    }
                    return $object_id_res;
                }
            }
            return true;
        }
        return false;
    }

    public function checkByRoleModelWrite($model,$object_id)
    {
        $user_role_id = ApiHelper::getUserRoleById();
        $permissions = $this->model::where('role_id',$user_role_id)
            ->where('model',$model)
            ->where(function ($q){
                $q->where('can_write',1)
                    ->orWhere('can_write',2);
            })
            ->first();

        if ($permissions){
            if ($permissions->can_write == 2){
                $permission_object_repository = new PermissionObjectRepository(PermissionObject::class);
                $check_permission_objects = $permission_object_repository->checkWrite($permissions->id,$object_id);

                return $check_permission_objects;
            }
            return true;
        }
        return false;
    }

    public function validateCustom($data)
    {
        $validator = Validator::make($data,$this->rule);
        if( $validator->fails() ) {
            return $this->response->sendError('Find error',$validator->errors(),1);
        }

        return $data;
    }
}
