<?php

namespace App\Repositories;

use App\Models\PermissionResources;
use Illuminate\Support\Facades\DB;

class PermissionObjectRepository extends BaseRepository
{
    public function __construct($model)
    {
        /**
         * Присвоение модели с которой будет работа
         */
        $this->model = $model;
        /**
         * Инициализация правил для валидации
         */
        $this->rule();
        /**
         * Инициализация BaseController для отправки ответов
         */
        $this->initialize_response();
    }

    /**
     * @param null $rule
     * установка правил валидации
     */
    public function rule($rule = null)
    {
        if ($rule == null) {
            $this->rule = [
                "permission_resources_id" => "required|integer",
                "can_read" => "required|integer",
                "can_write" => "required|integer",
                "object_id" => "required|integer",
            ];
        }else{
            $this->rule = $rule;
        }
    }

    public function getByResourceId($id)
    {
        $result = $this->model::where('permission_resources_id',$id)->get();
        return $result;
    }

    protected function checkPermissionResources($id)
    {
        $permission_resources_repo = new PermissionResourcesRepository(PermissionResources::class);
        $check_permission_resources = $permission_resources_repo->find($id);

        return $check_permission_resources;
    }

    protected function checkObject($obj,$id)
    {
        $check_object = $obj->model::find($id);

        if (!$check_object) {
            return $this->response->sendError('Find error', __('messages.not_find_model', [ 'model' => $obj->model , "id" => $id ] ) ,1);
        }

        return true;
    }

    public function checkAndCreate(array $data)
    {
        $check_permission = $this->model::where('permission_resources_id',$data["permission_resources_id"])->where('object_id' , $data["object_id"])->count();
        if ($check_permission > 0) {
            return $this->response->sendError('Find error',__('messages.model_exist', [ 'model' => 'Permission Object', 'param'=>' permission_resources_id '.$data["permission_resources_id"].' and object_id '.$data["object_id"] ] ),1);
        }

        $check_permission_resources = $this->checkPermissionResources($data["permission_resources_id"]);

        $check_object = $this->checkObject($check_permission_resources,$data["object_id"]);

        $permission = $this->create($data);

        return $this->find($permission->id);
    }

    public function checkAndUpdate($id, array $data)
    {
        $permission = $this->find($id);

        $check_permission_resources = '';

        if (isset($data["permission_resources_id"])) {
            $check_permission_resources = $this->checkPermissionResources($data["permission_resources_id"]);
        }else{
            $check_permission_resources = $permission->permission_resources_id;
        }

        if (isset($data["object_id"])) {
            $check_object = $this->checkObject($check_permission_resources,$data["object_id"]);
        }

        $role = $this->update($id, $data,true);

        return $this->find($id);
    }

    public function checkAndDestroy($id)
    {
        $permission = $this->destroy($id,true);

        return $permission;
    }

    public function checkRead($permission_resource_id,$obj_id)
    {
        if (is_null($obj_id)) {
            $permissions = $this->model::select('object_id')
                ->where('permission_resources_id',$permission_resource_id)
                ->where('can_read',1)
                ->get();
        }else{
            $permissions = $this->model::where('permission_resources_id',$permission_resource_id)
                ->where('object_id',$obj_id)
                ->where('can_read',1)
                ->first();
        }

        if ($permissions) {
            if (is_null($obj_id)) {
                return $permissions;
            }
            return true;
        }
        return false;
    }

    public function checkWrite($permission_resource_id,$obj_id)
    {
        if (is_null($obj_id)) {
            $permissions = true;
        }else{
            $permissions = $this->model::where('permission_resources_id',$permission_resource_id)
                ->where('object_id',$obj_id)
                ->where('can_write',1)
                ->first();
        }

        if ($permissions) {
            if (is_null($obj_id)) {
                return $permissions;
            }
            return true;
        }
        return false;
    }

    public function deleteAllByResources($permissions_resources_id)
    {
        DB::table('permission_objects')->where('permission_resources_id', $permissions_resources_id)->delete();
    }

    public function getAllByResources($id){
        $permissions = $this->model::where('permission_resources_id',$id)->get();
        return $permissions;
    }
}
