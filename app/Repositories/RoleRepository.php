<?php


namespace App\Repositories;


use App\Helpers\ApiHelper;
use App\Models\PermissionResources;
use App\Models\User;

class RoleRepository extends BaseRepository
{
    public function __construct($model)
    {
        /**
         * Присвоение модели с которой будет работа
         */
        $this->model = $model;
        /**
         * Инициализация правил для валидации
         */
        $this->rule();
        /**
         * Инициализация BaseController для отправки ответов
         */
        $this->initialize_response();
    }

    /**
     * @param null $rule
     * установка правил валидации
     */
    public function rule($rule = null)
    {
        if ($rule == null) {
            $this->rule = [
                "name" => "required",
            ];
        }else{
            $this->rule = $rule;
        }
    }

    public function checkAndCreate($data)
    {
        $role = $this->create($data);

        $permission_resource_repo = new PermissionResourcesRepository(PermissionResources::class);
        $data_create_permission_resources = [
            [
                "name" => "Users",
                "model" => "App\\Models\\User",
                "role_id" => $role->id,
                "can_read" => 1,
                "can_write" => 0,
            ]
        ];

        foreach ($data_create_permission_resources as $data_create_permission_resource ) {
            $permission_resource_repo->checkAndCreate($data_create_permission_resource);
        }

        return $this->find($role->id);
    }

    public function checkAndUpdate($id, $data)
    {
        $this->find($id);

        $role = $this->update($id, $data,true);

        return $this->find($id);
    }

    public function checkAndDestroy($id)
    {

        $role = $this->destroy($id,true);

        $users = User::where('role_id',$id)->get();

        foreach ($users as $user){
            $user->role_id = 2;
            $user->save();
        }

        return $role;
    }

    public function allCustom()
    {
        $role = $this->model::where('company_id', ApiHelper::getCompanyId())->orWhere('company_id', null)->get();

        return $role;
    }

    public function findCustom($id)
    {
        $role = $this->model::where('id', $id)
            ->where(function($q){
                $q->where('company_id', null)
                    ->orWhere('company_id', ApiHelper::getCompanyId());
            })
            ->first();
        if (empty($role)){
            return $this->response->sendError('Find error', __('messages.not_find_model', [ 'model' => $this->model , 'id' =>$id ] ) ,1);
        }

        return $role;
    }
}
