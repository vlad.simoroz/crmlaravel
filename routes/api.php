<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->namespace('Api')->group( function () {
    /**
     * Users Route
     */
    Route::get('users/current','UserController@current');
    Route::post('users/{id}/img','UserController@update_img');
    Route::resource('users', 'UserController');

    /**
     * Company Route
     */
    Route::post('company/{id}/img','CompanyController@update_img');
    Route::get('company/all','CompanyController@getAll');
    Route::resource('company','CompanyController');

    /**
     * Permission Resources Route
     */
    Route::get('permission_resources/getByRole/{id}', 'PermissionResourcesController@getByRole');
    Route::post('permission_resources/update', 'PermissionResourcesController@updateMass');
    Route::resource('permission_resources', 'PermissionResourcesController');

    /**
     * App Config Route
     */
    Route::resource('app_config','AppConfigController');

    /**
     * Permission Object Route
     */
    Route::resource('permission_object', 'PermissionObjectController');

    /**
     * Role Route
     */
    Route::resource('roles', 'RoleController');

    /**
     * RequestLog Routing
     */
    Route::get('request_log','RequestLogController@index');
    Route::post('request_log','RequestLogController@filter');

});
