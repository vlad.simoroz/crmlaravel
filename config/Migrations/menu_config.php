<?php
    return [
        ["id" => 1, 'route' => '/home' , "name" => "Home" , "company_id" => "1" ],
        ["id" => 2, 'route' => '/locations' , "name" => "Locations" , "company_id" => "1" ],
        ["id" => 3, 'route' => '/panels' , "name" => "Panels" , "company_id" => "1" ],
        ["id" => 4, 'route' => '/analytics' , "name" => "Analytics" , "company_id" => "1" ],
        ["id" => 5, 'route' => '/users' , "name" => "Users" , "company_id" => "1" ],
        ["id" => 6, 'route' => '/userlog' , "name" => "User log" , "company_id" => "1" ],
        ["id" => 7, 'route' => '/settings' , "name" => "Settings" , "company_id" => "1" ],
        ["id" => 8, 'route' => '/debugging' , "name" => "Debugging" , "company_id" => "1" ],
        ["id" => 9, 'route' => '/messages' , "name" => "Messages" , "company_id" => "1" ],
    ];
