<?php

return [
    // Admin
    ["id" => 1, 'name' => 'Locations', 'role_id' => 1, 'model' => 'App\Models\Location', 'can_read' => 1, 'can_write' => 1,],
    ["id" => 2, 'name' => 'Users', 'role_id' => 1, 'model' => 'App\Models\User', 'can_read' => 1, 'can_write' => 1,],
    ["id" => 3, 'name' => 'Panels', 'role_id' => 1, 'model' => 'App\Models\Panel', 'can_read' => 1, 'can_write' => 1,],
    ["id" => 7, 'name' => 'Menu', 'role_id' => 1, 'model' => 'App\Models\MenuConfig', 'can_read' => 1, 'can_write' => 1,],
    //User
    ["id" => 4, 'name' => 'Locations', 'role_id' => 2, 'model' => 'App\Models\Location', 'can_read' => 1, 'can_write' => 0,],
    ["id" => 5, 'name' => 'Users', 'role_id' => 2, 'model' => 'App\Models\User', 'can_read' => 1, 'can_write' => 0,],
    ["id" => 6, 'name' => 'Panels', 'role_id' => 2, 'model' => 'App\Models\Panel', 'can_read' => 1, 'can_write' => 0,],
    ["id" => 8, 'name' => 'Menu', 'role_id' => 2, 'model' => 'App\Models\MenuConfig', 'can_read' => 1, 'can_write' => 1,],
];
